import logo from './logo.svg';
import './App.css';
import CustomButton from './components/button';
import Grid from './components/grid';

const onClick = () => {
  console.log("p")
}
function App() {
  return (
    <div>
      <div>
        <Grid></Grid>
      </div>
      <div style={{textAlign: "center"}}>
        <CustomButton color={"green"} text={"Start"} onClick={onClick}/>
        <CustomButton color={"red"} text={"Reset"} onClick={onClick}/>
      </div>
    </div>
  );
}

export default App;
