import React from "react";

const Grid = () => {

    const gridSize = 4;

    const cells = []

    for (let yIndex = gridSize-1; yIndex >= 0; yIndex -= 1) {
        for (let xIndex = 0; xIndex < gridSize; xIndex += 1) {
            cells.push(<div key={`${xIndex},${yIndex}`} className={`grid-item`}>{`${xIndex},${yIndex}`}</div>);
          }
    }

    return <div className="grid-container" style={{gridTemplateColumns: "auto ".repeat(gridSize)}}>{cells}</div>;
  }
  
export default Grid;
  